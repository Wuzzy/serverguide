# version: 5
# author(s):
# reviewer(s):
# textdomain: serverguide


# general
Server Guide=
don't feel lost!=

Introduction=
Parties=
Rules=
Server Rules=
Mods=
Info=
Server Info=


# INTRODUCTION
HOW TO PLAY=
Look for the specific signs in order to enter a minigame, and left-click them to join the match (or the queue).=
Some minigames allow you to enter an ongoing match (i.e. Block League), while as for others you'll have to wait the end of the match (i.e. Murder).=
LEAVE A GAME=
In order to leave any minigame and return back to the lobby, just open the chat and do /quit=




# PARTIES
WANT TO PLAY IN COMPANY? USE PARTIES!=
When you're in a party, only the party leader can enter a game, and when they do, the rest of the group is automatically added.=
This is great to play with friends, as you'll all be put in the same team (if the map supports teams).=
Finally, parties also have a separate chat, only visible to their members.=

COMMANDS=
/party invite <player>: invites a player into the party=
/party join: accepts the invite to a party=
/party leave: leaves the party=
/party disband: disbands the party (only the party leader can do that)=
/p <message>: sends a message in the party chat=



# RULES
1) Always respect other players=
2) No excessive swearing=
3) No modified clients=

Admins reserve the right to remove whoever transgress the rules. In case of repeated misbehaviours, the user will be banned=



# MOD
Did you know that most of the mods here in the server have been made by us? And they're all open source!=
Our goal is to make them lighter and well-performing, yet at the same time solid and funny: look them up on gitlab.com/zughy-friends-minetest to know more about it=



# INFO SERVER
A.E.S. is a server made by a group of guys in their free time, currently geolocalised in Italian and English.=
It's one of the projects of Etica Digitale (an Italian group divulging about digital ethics) and it's born as a place where to play and have fun, in the full respect of privacy and human rights.=

WHAT DO YOU MEAN?=
PRIVACY=
Do you know those ads which are so invasive that sometimes it almost feels like they're spying on you? There. =
This practice of following the user from the ground up is actually older than we think, and it was originated in the year 2000, =
after Google decided to survive an economic bubble by collecting more data than what it actually needed. =
Though it promised this was only going to be temporary, Google didn't change this behaviour, and on the contrary, more and more companies (including government enterprises) started doing the same =
They started adopting the same approach because... well, there was no law to prohibit it. =
Facebook, Amazon, Microsoft, even some network providers like Verizon (today's Oath) saw it as a golden goose, =
and it didn't really matter that this would have led to see people not as human being anymore, but as numbers.=
Today's situation has become so paradoxical that even those online newspapers that do address the issue, have services like Google Analytics running in background to collect new data. =
And even schools, though being a public environment, use services provided by these companies - Teams, Meet, Classroom, Zoom etc.=

The world of videogames is no exception, 2020 has indeed reached a new level of monitoring: =
Valorant, a game by Riot Games, installs an anticheat which, to assure no one is cheating, automatically starts as the computer is turned on. =
The anticheat, which has been shown to be crossable anyways, has COMPLETE ACCESS to the computer, so much that it falls under the definition of 'rootkit': =
a set of tools used to hack, which allows to gain the complete control of the infected PC.=

WHAT WE HAVE DONE=
A.E.S. runs on an engine (Minetest) which doesn't dispose of an actual account: =
this means that your data aren't stored in a central server held by some company (as opposed to MineCRAFT which has them held by Microsoft) and your emails aren't associated to any account. =
In other words, we couldn't steal your data even if we wanted to.=

On the other hand, as far as transparency is concerned, each mod of the server is free software: =
as for the security level, this means that anyone who knows about it, can analyse the code of the mod and tell you if it's actually doing what it says (and it comes with other perks too!).=

As for the anticheat: adopting such tools like Valorant did, must never be the solution. The user musn't be spied. If something, bypassed=

HUMAN RIGHTS=
Nowadays we condemn events like the Holocaust and the millions of deaths it has brutally caused. =
Information channels and politics, however, though repeting phrases like "never again", seem to ignore what's happening in one of today's biggest economic powers in the world, which has little to envy to the Holocaust: China. =
For the past 70 years, the Chinese Communist Party has used concentration camps (laojiao), censorship, and extreme levels of surveillance, to nip any voice of dissent in the bud, without any trace of respect for human rights. =
Xinjiang region is compared to a digital gym-jail, Tibet doesn't allow independent visitors since 2008, internet is completely controlled and the last events in Hong Kong speak for themselves.=

This situation makes the life of Chinese citizens and minorities in China - like the Uyghur one - resemble that of working animals, and in no way this should be tolerated. =
Many Western companies though, don't seem to be very sensitive about it and, to make money, they are willing to look the other way.=

And again, the world of videogames is not an exception: in 2015 the American company Riot Games has passed under the management of Chinese company Tencent, =
while in 2019 Blizzard had no problem suspending a player from Hong Kong for supporting his country during a tournament, thus messing with the company's earnings in China.=

If a Chinese citizen isn't given much choice on how to behave, the same doesn't apply to Western companies. And it's only fair to let it be known what position did certain companies decide to take. =
Without going around and around with it, shopping on these games is equivalent to economically support a dictatorship.=

We said: enough. A.E.S. was born also to give space and tools to those who don't want to support certain realities. =
The road is long and narrow, but it doesn't matter: we won't submit to this inhumanity.=
Games like Minetest take power and authority away from these companies, because they are created by single people, hence they can be personalised and run on each person's homemade server. =
There's no need to come to terms with monsters anymore.=
